using Documenter
using NDLiterals
using Test

DocMeta.setdocmeta!(
	NDLiterals,
	:DocTestSetup,
	:(using NDLiterals);
	recursive = true,
)
doctest(NDLiterals)

@testset "NDLiterals" begin
	let array = zeros(Int, 4, 2, 3)
		array[:, :, 1] = [
			1 2;
			3 4;
			5 6;
			7 8;
		]
		array[:, :, 2] = [
			11 22;
			33 44;
			55 66;
			77 88;
		]
		array[:, :, 3] = [
			111 222;
			333 444;
			555 666;
			777 888;
		]

		@test array == @ndliteral [
			[
				[1, 2],
				[3, 4],
				[5, 6],
				[7, 8],
			],
			[
				[11, 22],
				[33, 44],
				[55, 66],
				[77, 88],
			],
			[
				[111, 222],
				[333, 444],
				[555, 666],
				[777, 888],
			],
		]
	end

	let array = zeros(Int, 3, 1, 2, 1)
		array[:, :, 1, 1] = [
			1;
			2;
			3;
		]
		array[:, :, 2, 1] = [
			11;
			22;
			33;
		]

		@test array == @ndliteral [
			[
				[
					[1],
					[2],
					[3],
				],
				[
					[11],
					[22],
					[33],
				],
			],
		]
	end

	let array = zeros(1, 1, 1, 1, 1)
		array[1, 1, 1, 1, 1] = 1.0

		@test array == @ndliteral [[[[[1.0]]]]]
	end

	let array = Array{Any}(undef, 2, 3)
		array[1, :] = [97, :b, 'c']
		array[2, :] = ["1, 2", :([1, 2]), [1, 2]]

		@test array == @ndliteral [
			[97, :b, 'c'],
			["1, 2", :([1, 2]), Int[1, 2]],
		]
	end

	let array = Array{Any}(undef, 2, 3)
		array[1, :] = [97, :b, 'c']
		array[2, :] = ["1, 2", :([1, 2]), [1, 2]]

		@test array == @ndliteral [
			[97, :b, 'c'],
			["1, 2", :([1, 2]), interpolate([1, 2])],
		]
	end

	let array = Array{Any}(undef, 1, 2)
		array[1, :] = [1, [2, 3]]

		@test array == @ndliteral [
			[1, Int[2, 3]],
		]
	end

	let array = Array{Any}(undef, 1, 2)
		array[1, :] = [1, [2, 3]]

		@test array == @ndliteral [
			[1, interpolate([2, 3])],
		]
	end

	@test_throws ErrorException NDLiterals.flatten(:([
		2,
		[1, 2, 3],
	]))

	@test_throws ErrorException NDLiterals.flatten(:([
		[1, 2],
		[1, 2, 3],
	]))

	@test_throws ErrorException NDLiterals.flatten(:([
		[
			[1, 2],
			[3, 4],
		],
		[
			[5, 6],
			[7],
		],
	]))

	@test_throws ErrorException NDLiterals.flatten(:([
		[
			[1, 2],
			[3, 4],
		],
		[
			[5],
			[7, 8],
		],
	]))

	@test_throws ErrorException NDLiterals.flatten(:([
		[
			[1, 2],
		],
		[
			[5, 6],
			[7, 8],
		],
	]))

	@test_throws ErrorException NDLiterals.flatten(:(1))

	@test_throws ErrorException NDLiterals.flatten(:([]))

	@test_throws ErrorException NDLiterals.flatten(:([1, 2, 3]))

	@test_throws ErrorException NDLiterals.flatten(:([[[]]]))
end

@testset "Benchmarks" begin
	let inc_bench(bench, file) = include(joinpath("benches", bench, file))
		let inc(file) = inc_bench("24_28_4_3", file)
			@test inc("assignments.jl") == inc("ndliteral.jl")
		end

		let inc(file) = inc_bench("120_32_9", file)
			@test inc("assignments.jl") == inc("ndliteral.jl")
		end

		let inc(file) = inc_bench("2_6_3_5_4", file)
			@test inc("assignments.jl") == inc("ndliteral.jl")
		end

		let inc(file) = inc_bench("3_2_3", file)
			@test inc("assignments.jl") == inc("ndliteral.jl")
		end
	end
end
