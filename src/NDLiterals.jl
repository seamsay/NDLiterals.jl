"""
Macros for writing N-dimensional array literals in source code.

Julia provides support for 1-D (vector) and 2-D (matrix) array literals, but does not
have any way to write literals of higher dimensions. Furthermore matrices that contain
vectors (admittedly a far less common need) can be difficult to write, requiring an awkward
combination of `permutedims` and `hcat`. This module provides a macro, [`@ndliteral`](@ref),
that solves both these problems.

The macro parses a tree of vector literals and generates an appropriate call to `reshape`.

```jldoctest
julia> @ndliteral [
           [
               [1.1, 2.2, 3.3, 4.4],
               [5.5, 6.6, 7.7, 8.8],
           ],
           [
               [11, 22, 33, 44],
               [55, 66, 77, 88],
           ],
       ]
2×4×2 Array{Float64,3}:
[:, :, 1] =
 1.1  2.2  3.3  4.4
 5.5  6.6  7.7  8.8

[:, :, 2] =
 11.0  22.0  33.0  44.0
 55.0  66.0  77.0  88.0
```

The arrays are written in row-major style, which will be familiar to NumPy users, then
converted into the column-major style that Julia uses. To do this the macro treats vector
literals as defining a new dimension, which means that you can't use a vector literal to
denote an actual vector (because 99 times out of 100 that will only happen when you make a
mistake).

```jldoctest
julia> @ndliteral [
           [1, [2, 3]],
           [4, 5],
       ]
ERROR: LoadError: expected vector literal at index [1, 1]
[...]
```

It's worth noting that this doesn't work with matrix literals either.

```jldoctest
julia> [
           1 [2 3];
           4 5;
       ]
ERROR: ArgumentError: number of columns of each array must match (got (3, 2))
[...]

julia> [
           1 [2, 3];
           4 5;
       ]
ERROR: DimensionMismatch("mismatch in dimension 1 (expected 1 got 2)")
[...]

julia> [
           1 [2; 3];
           4 5;
       ]
ERROR: DimensionMismatch("mismatch in dimension 1 (expected 1 got 2)")
[...]

julia> [
           1 Int[2, 3];
           4 5;
       ]
ERROR: DimensionMismatch("mismatch in dimension 1 (expected 1 got 2)")
[...]

julia> let
           id(x) = x
           [
               1 id([2, 3]);
               4 5;
           ]
       end
ERROR: DimensionMismatch("mismatch in dimension 1 (expected 1 got 2)")
[...]

julia> permutedims(hcat([1, [2, 3]], [4, 5]))
2×2 Array{Any,2}:
 1   [2, 3]
 4  5
```

Whereas with `@ndliteral` you can simply provide an element type for the array

```jldoctest
julia> @ndliteral [
           [1, Int[2, 3]],
           [4, 5],
       ]
2×2 Array{Any,2}:
 1   [2, 3]
 4  5
```

or use [`interpolate`](@ref) if you want to retain type inference.

```jldoctest
julia> @ndliteral [
           [1, interpolate([2, 3])],
           [4, 5],
       ]
2×2 Array{Any,2}:
 1   [2, 3]
 4  5
```

The macro will also take a type if you want to override the inferred type.

```jldoctest
julia> @ndliteral Float32 [
           [1.2, 3.4],
           [5.6, 7.8],
       ]
2×2 Array{Float32,2}:
 1.2  3.4
 5.6  7.8
```

See also: [`@ndliteral`](@ref), [`interpolate`](@ref)
"""
module NDLiterals


using DataStructures


export @ndliteral, interpolate


"""
    interpolate(x)

Interpolate a vector literal into the array.

This function prevents [`@ndliteral`](@ref) from treating a vector literal as a new
dimension in the array, so that you can include an _inferred_ vector in the array (if you
don't need inference you can just give the array an element type).

# Examples

```jldoctest
julia> @ndliteral [
           [1, interpolate([1, 2])],
       ]
1×2 Array{Any,2}:
 1  [1, 2]
julia> @ndliteral [
           [1, Int8[1, 2]],
       ]
1×2 Array{Any,2}:
 1  Int8[1, 2]
```
"""
interpolate(x) = x


isvect(expr) = Meta.isexpr(expr, :vect)
isrow(expr) = !any(isvect.(expr.args))


function flatten(array)
	if !isvect(array)
		error("argument is not a vector literal")
	end

	vector = []

	sizes = DefaultDict{Int, Vector{Int}}(() -> [])

	queue = Queue{Tuple{Vector{Int}, Expr}}()
	enqueue!(queue, ([], array))

	while !isempty(queue)
		index, node = dequeue!(queue)
		dimension = length(index) + 1

		@assert isvect(node) "leaf node pushed onto stack"

		if isempty(node.args)
			error("item at index $index is an empty vector")
		elseif !all(isvect.(node.args))
			i = findfirst(!isvect, node.args)
			error("expected vector literal at index $(vcat(index, i))")
		elseif any(isempty(child.args) for child in node.args)
			i = findfirst(c -> isempty(c.args), node.args)
			error("item at index $(vcat(index, i)) is an empty vector")
		# Because Julia's arrays are column-major, we need to essentially
		# swap rows and columns. So when all of the child args are rows we
		# need to pick an item from each row before moving onto the next
		# index in the row.
		elseif all(isrow(child) for child in node.args)
			push!(sizes[dimension + 1], length(node.args))

			dimensions = length.(child.args for child in node.args)
			append!(sizes[dimension], dimensions)

			for i in 1:minimum(dimensions)
				for array in node.args
					push!(vector, array.args[i])
				end
			end
		elseif all(all(isvect.(child.args)) for child in node.args)
			push!(sizes[dimension], length(node.args))
			for (i, child) in enumerate(node.args)
				enqueue!(queue, (vcat(index, i), child))
			end
		else
			i = findfirst(!isrow, node.args)
			if i !== nothing
				j = findfirst(!isvect, node.args[i].args)
				if j !== nothing
					error("expected vector literal at index $(vcat(index, i, j))")
				end
			end

			error("unknown issue at index $index")
		end
	end

	dimensions = zeros(Int, maximum(keys(sizes)))

	for i in keys(sizes)
		unique!(sizes[i])

		@assert !isempty(sizes[i]) "empty dimension not caught during tree-traversal"

		if length(sizes[i]) > 1
			error("dimension $i does not have a consistent shape ($(sizes[i]))")
		else
			dimensions[i] = sizes[i][1]
		end
	end

	vector, reverse(dimensions)
end


function _ndliteral!(vector_expr, array)
	vector, dimensions = flatten(array)

	append!(vector_expr.args, vector)

	expr = Expr(:call)
	push!(expr.args, :reshape)
	push!(expr.args, vector_expr)
	append!(expr.args, dimensions)

	expr
end

"""
    @ndliteral [...]

Interpret a tree of vector literals as an N-dimensional array.

See the module-level documentation ([`NDLiterals`](@ref)) for a more in-depth explanation.

# Unsupported Arguments

0-D and 1-D arrays are not supported because these are trivial to write in base Julia (and
not supporting them makes the code much more elegant).
"""
macro ndliteral(array)
	expr = Expr(:vect)
	_ndliteral!(expr, array)
end

"""
    @ndliteral type [...]

Interpret a tree of vector literals as an N-dimensional array with an element type.

See the module-level documentation ([`NDLiterals`](@ref)) for a more in-depth explanation.

# Unsupported Arguments

0-D and 1-D arrays are not supported because these are trivial to write in base Julia (and
not supporting them makes the code much more elegant).
"""
macro ndliteral(type, array)
	expr = Expr(:ref)
	push!(expr.args, type)
	_ndliteral!(expr, array)
end

end # module
