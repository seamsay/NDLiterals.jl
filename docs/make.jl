using Documenter, NDLiterals

DocMeta.setdocmeta!(
    NDLiterals,
    :DocTestSetup,
    :(using NDLiterals);
    recursive = true,
)

makedocs(;
    modules = [NDLiterals],
    format = Documenter.HTML(),
    pages = [
        "Home" => "index.md",
    ],
    repo = "https://gitlab.com/seamsay/NDLiterals.jl/blob/{commit}{path}#L{line}",
    sitename = "NDLiterals.jl",
    authors = "Sean Marshallsay",
)
