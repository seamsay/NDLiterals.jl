# NDLiterals

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://seamsay.gitlab.io/NDLiterals.jl/dev)
[![Build Status](https://gitlab.com/seamsay/NDLiterals.jl/badges/master/pipeline.svg)](https://gitlab.com/seamsay/NDLiterals.jl/pipelines)
[![Coverage](https://gitlab.com/seamsay/NDLiterals.jl/badges/master/coverage.svg)](https://gitlab.com/seamsay/NDLiterals.jl/commits/master)

A macro that allows you to write array literals of any dimension directly into your source code.

All documentation is in the module itself, please see the documentation link above or (if you've already installed the package) run

```sh
julia -e 'using NDLiterals; display(@doc NDLiterals); println()'
```

to read the documentation offline.
